package datasrc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/jszwec/csvutil"
)

const (
	baseURL            = "https://api.discogs.com"
	userAgent          = "J11337Purchases/1.0"
	maxPerPage         = 100
	defaultReleaseSort = "title"
	dumpDir            = "./dumps"
)

var client http.Client

type pagination struct {
	PerPage int64 `json:"per_page"`
	Pages   int64 `json:"pages"`
	Page    int64 `json:"page"`
	Items   int64 `json:"items"`
	URLs    struct {
		Next string `json:"urls.next"`
		Last string `json:"urls.last"`
	}
}
type discogsReleaseArtist struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Join string `json:"join"`
	URL  string `json:"resource_url"`
}

type discogsReleaseLabel struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	CatNo string `json:"catno"`
	URL   string `json:"resource_url"`
}

type discogsReleaseFormats struct {
	Quantity     string   `json:"qty"`
	Name         string   `json:"name"`
	Descriptions []string `json:"descriptions"`
}

type discogsReleaseBasicInformation struct {
	ID            int64                   `json:"id"`
	Title         string                  `json:"title"`
	Year          int                     `json:"year"`
	ThumbnailURL  string                  `json:"thumb"`
	CoverImageURL string                  `json:"cover_image"`
	Artists       []discogsReleaseArtist  `json:"artists"`
	Labels        []discogsReleaseLabel   `json:"labels"`
	Formats       []discogsReleaseFormats `json:"formats"`
	Genres        []string                `json:"genres"`
	Styles        []string                `json:"styles"`
}

type discogsRelease struct {
	ID               int64                          `json:"id"`
	InstanceID       int64                          `json:"instance_id"`
	BasicInformation discogsReleaseBasicInformation `json:"basic_information"`
}

type discogsFolder struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	URL      string `json:"resource_url"`
	Count    int    `json:"count"`
	Releases []discogsRelease
}

type discogsFolders []discogsFolder
type discogsFolderReleases []discogsRelease

type discogsCollectionResponse struct {
	Folders discogsFolders `json:"folders"`
}

type discogsCollectionReleasesResponse struct {
	Releases   discogsFolderReleases `json:"releases"`
	Pagination pagination            `json:"pagination"`
}

type discogsReleaseCSV struct {
	ID      int64  `csv:"id"`
	Title   string `csv:"title"`
	Artists string `csv:"artists"`
	Format  string `csv:"format"`
	Genres  string `csv:"genres"`
}

func getDiscogsPage(reqURL, sort string, page int64) ([]byte, error) {
	return execute(http.MethodGet, fmt.Sprintf("%v?per_page=%v&page=%v&sort=%v", reqURL, maxPerPage, page, sort))
}

func getDiscogsFolderReleases(username string, folderID int64) discogsFolderReleases {
	// /users/{username}/collection/folders/{folder_id}/releases
	reqURL := fmt.Sprintf("%v/users/%v/collection/folders/%v/releases", baseURL, username, folderID)
	hasNextPage := true
	var page int64 = 1

	var releases discogsFolderReleases

	for hasNextPage {
		data, err := getDiscogsPage(reqURL, defaultReleaseSort, page)
		handleError(err)

		r := discogsCollectionReleasesResponse{}
		err = json.Unmarshal(data, &r)
		handleError(err)

		releases = append(releases, r.Releases...)
		hasNextPage = int64(len(releases)) < r.Pagination.Items
		page = r.Pagination.Page + 1
	}
	return releases
}

func getDiscogsCollectionFolders(username string) discogsFolders {
	// /users/{username}/collection/folders
	reqURL := fmt.Sprintf("%v/users/%v/collection/folders", baseURL, username)
	data, err := execute(http.MethodGet, reqURL)
	handleError(err)

	r := discogsCollectionResponse{}
	err = json.Unmarshal(data, &r)

	handleError(err)
	return r.Folders
}

func execute(requestType, url string) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest(requestType, url, nil)
	handleError(err)

	req.Header.Set("User-Agent", userAgent)
	resp, err := client.Do(req)
	handleError(err)

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

func handleError(err error) {
	// TODO: need a logger / handler
	if err != nil {
		log.Fatal(err)
	}
}

func newReleaseCSV(r discogsRelease) discogsReleaseCSV {
	var artistNames []string
	for _, a := range r.BasicInformation.Artists {
		artistNames = append(artistNames, a.Name)
	}

	var formats []string
	for _, a := range r.BasicInformation.Formats {
		formats = append(formats, a.Name)
	}

	return discogsReleaseCSV{
		ID:      r.BasicInformation.ID,
		Title:   r.BasicInformation.Title,
		Artists: strings.Join(artistNames, ";"),
		Format:  strings.Join(formats, ";"),
		Genres:  strings.Join(r.BasicInformation.Genres, ";"),
	}
}

func getDiscogsUsersReleases(username string) discogsFolderReleases {
	folders := getDiscogsCollectionFolders(username)

	var releases discogsFolderReleases
	for _, f := range folders {
		releases = getDiscogsFolderReleases(username, f.ID)
	}
	return releases
}

func (dr discogsRelease) discogsAsRelease() Release {
	var artists []Artist
	for _, da := range dr.BasicInformation.Artists {
		artists = append(artists, Artist{
			ID:   da.ID,
			Name: da.Name,
		})
	}

	var labels []Label
	for _, la := range dr.BasicInformation.Labels {
		labels = append(labels, Label{
			ID:   la.ID,
			Name: la.Name,
		})
	}

	return Release{
		ID:      dr.BasicInformation.ID,
		Title:   dr.BasicInformation.Title,
		Artists: artists,
		Genres:  dr.BasicInformation.Genres,
		Labels:  labels,
		Source:  SOURCE_DISCOGS,
	}
}

func (releases discogsFolderReleases) discogsAsReleases() []Release {
	var r []Release
	for _, dr := range releases {
		r = append(r, dr.discogsAsRelease())
	}
	return r
}

func (r discogsFolderReleases) toCSV() {
	var rCSVData []discogsReleaseCSV

	for _, rawRelease := range r {
		rCSVData = append(rCSVData, newReleaseCSV(rawRelease))
	}

	rCSV, err := csvutil.Marshal(rCSVData)
	handleError(err)

	err = ioutil.WriteFile(fmt.Sprintf("%v/releases.csv", dumpDir), rCSV, 0644)
	handleError(err)
}

func (r discogsFolderReleases) toJSON() {
	rJSON, err := json.Marshal(r)
	handleError(err)

	err = ioutil.WriteFile(fmt.Sprintf("%v/releases.json", dumpDir), rJSON, 0644)
	handleError(err)
}

func dump(releases discogsFolderReleases) {
	fmt.Printf("\nReturned %v releases\n", len(releases))
	if _, err := os.Stat(dumpDir); !os.IsNotExist(err) {
		handleError(err)

		err = os.RemoveAll(dumpDir)
		handleError(err)
	}
	os.Mkdir(dumpDir, 0777)
	releases.toJSON()
	releases.toCSV()
}
