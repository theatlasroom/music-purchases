package datasrc

import (
	"fmt"
)

type source int

const (
	SOURCE_DISCOGS source = iota
)

type Artist struct {
	ID   int64
	Name string
}

type Label struct {
	ID   int64
	Name string
}

type Release struct {
	ID      int64
	Title   string
	Genres  []string
	Artists []Artist
	Labels  []Label
	Source  source
}

type Releases []Release

func GetDiscogsReleases(username string) []Release {
	fmt.Printf("Fetching discogs releases for %v", username)
	releases := getDiscogsUsersReleases(username)
	return releases.discogsAsReleases()
}

// TODO: we should
// Takes a bandcamp release and resolves the discogs metadata
// func resolveBandcampFromDiscogs(){ }
