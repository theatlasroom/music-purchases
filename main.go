package main

import (
	"fmt"

	src "gitlab.com/theatlasroom/music-purchases/datasrc"
)

const (
	username = "junior1z1337"
	dumpDir  = "./dumps"
)

func main() {
	discogs := src.GetDiscogsReleases(username)
	fmt.Println(discogs)
}
